import time
from datetime import date, datetime
import datetime


class Screenshots:

    def __init__(self, driver):
        self.driver = driver
        self.today = date.today()
        self.date_hour = datetime.datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")

        self.screenshot_Rodo = r"D:\Lukasz\Tvn_24_2021\Report\Screen_shots\Rodo"

    def rodo_main_page_screenshot(self):
        driver = self.driver
        screenshot_name = f'Rodo_main_page_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.screenshot_Rodo}\{screenshot_name}')
        print(f"Screenshot of Rodo saved as : {screenshot_name}")
        print(self.date_hour)
