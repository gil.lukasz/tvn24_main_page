import allure
from allure_commons.types import AttachmentType
from datetime import date, datetime
import datetime
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as Wait
from Video.Screenshots_Video_articles import Video_Screenshots as ScreenShot


class Video_check:


    def __init__(self, driver):
        self.driver = driver

        self.date_hour = datetime.datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")

        self.ID_rodo_accept = (By.ID, "onetrust-accept-btn-handler")
        self.CSS_Video_mute_icon_V1 = "[data-test-id='nuvi-muted-autoplay-hovered-icon']>ellipse"
        self.CSS_Video_mute_icon_V2 = "div.vjs-autoplay-unmute-big-button"
        self.CSS_Video_mute_icon_V3 = "button[data-test-id='nuvi-big-play-button']"

        self.nuvi_control_bar = "div.article-story-header div[data-test-id='nuvi-control-bar']"
        self.rodo_accept = "onetrust-accept-btn-handler"
        self.Nuvi_Mute_icon = "[data-test-id='nuvi-muted-autoplay-hovered-icon']>ellipse"
        self.Mute_icon_V1 = "div.vjs-autoplay-unmute-big-button"
        self.Mute_icon_V2 = "button[data-test-id='nuvi-big-play-button']"
        self.nuvi_control_bar = "div.article-story-header div[data-test-id='nuvi-control-bar']"

    @allure.step("Otwarcie strony z Href")
    def open_page_from_href(self, href_link):
        driver = self.driver
        driver.get(href_link)

    @allure.step("Oczekiwanie Na Rodo")
    def article_rodo_accept(self):
        driver = self.driver
        attempt = 1
        for approach in range(5):
            try:
                accept_button = Wait(driver, 2, 0.5).until(EC.presence_of_element_located(self.ID_rodo_accept))
                # driver.execute_script("return arguments[0].scrollIntoView(true);", accept_button)
                # Screenshots(self.driver).rodo_main_page_screenshot()
                allure.attach(self.driver.get_screenshot_as_png(), name=f"Rodo_alert - {self.date_hour}",
                              attachment_type=AttachmentType.PNG)
                accept_button.click()
                break
            except TimeoutException:
                attempt += 1
                continue
        else:
            print(f'Rodo Approval - after {attempt} attempt was not displayed')


    @allure.step("Video presence check")
    def Video_presence_all(self):
        driver = self.driver
        counter = 1
        for attempt in range(10):
            print(f"Video Presence - attempt : {counter}")
            if len(driver.find_elements(By.CSS_SELECTOR, self.CSS_Video_mute_icon_V1)) > 0:
                Icon_V1 = driver.find_element(By.CSS_SELECTOR, self.CSS_Video_mute_icon_V1)
                driver.execute_script(
                    "return arguments[0].scrollIntoView({block: 'center', inline: 'center'});", Icon_V1)
                AC(driver).move_to_element(Icon_V1).perform()
                allure.attach(self.driver.get_screenshot_as_png(), name=f"Article_with_video",
                              attachment_type=AttachmentType.PNG)
                print(f"Video was found after {counter} attempt")
                break
            elif len(driver.find_elements(By.CSS_SELECTOR, self.CSS_Video_mute_icon_V2)) > 0:
                Icon_V2 = driver.find_element(By.CSS_SELECTOR, self.CSS_Video_mute_icon_V2)
                driver.execute_script(
                    "return arguments[0].scrollIntoView({block: 'center', inline: 'center'});", Icon_V2)
                AC(driver).move_to_element(Icon_V2).perform()
                allure.attach(self.driver.get_screenshot_as_png(), name=f"Article_with_video",
                              attachment_type=AttachmentType.PNG)
                print(f"Video was found after {counter} attempt")
                break
            elif len(driver.find_elements(By.CSS_SELECTOR, self.CSS_Video_mute_icon_V3)) > 0:
                Icon_V3 = driver.find_element(By.CSS_SELECTOR, self.CSS_Video_mute_icon_V3)
                driver.execute_script(
                    "return arguments[0].scrollIntoView({block: 'center', inline: 'center'});", Icon_V3)
                AC(driver).move_to_element(Icon_V3).perform()
                allure.attach(self.driver.get_screenshot_as_png(), name=f"Article_with_video",
                              attachment_type=AttachmentType.PNG)
                print(f"Video was found after {counter} attempt")
                break

            else:
                print(f"Video Presence - attempt : {counter} - FAILED")
                counter += 1
                time.sleep(2)
                continue
        else:
            print(f"No video appear after {counter} attempt")





    @allure.step("Weryfikacja obecnosci Video - Nuvi PLayer")
    def Nuvi_player_presence(self):

        driver = self.driver
        for attempt in range(5):
            try:
                Video_Player = Wait(driver, 2, 1).until(
                    EC.presence_of_element_located((By.TAG_NAME, "ellipse")))
                # driver.execute_script("return arguments[0].scrollIntoView(true);", Video_Player)
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Article_with_video - {self.date_hour}",
                              attachment_type=AttachmentType.PNG)
                # ScreenShot(self.driver).article_video_presence_check()
                # print(f"Nuvi player appeared")
                return True
            except TimeoutException:
                continue
        return False

    @allure.step("Weryfikacja obecnosci Video - PLayer")
    def video_player_presence(self):

        driver = self.driver
        for approach in range(5):
            try:
                Video_Player = Wait(driver, 3, 1).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, self.Mute_icon_V1)))
                # driver.execute_script("return arguments[0].scrollIntoView(true);", Video_Player)
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Article_with_video - {self.date_hour}",
                              attachment_type=AttachmentType.PNG)
                # print(f"Video player appeared")
                return True
            except TimeoutException:
                pass

            try:
                Video_Player = Wait(driver, 3, 1).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, self.Mute_icon_V2)))
                # driver.execute_script("return arguments[0].scrollIntoView(true);", Video_Player)
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Article_with_video - {self.date_hour}",
                              attachment_type=AttachmentType.PNG)
                # print(f"Video player appeared")
                return True
            except TimeoutException:
                pass

            try:
                Video_Player = Wait(driver, 3, 1).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, self.Nuvi_Mute_icon)))
                # driver.execute_script("return arguments[0].scrollIntoView(true);", Video_Player)
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Article_with_video - {self.date_hour}",
                              attachment_type=AttachmentType.PNG)

                # print(f"Video player appeared")
                return True
            except TimeoutException:
                pass

        return False
