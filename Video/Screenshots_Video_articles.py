import time
from datetime import date, datetime
import datetime


class Video_Screenshots:

    def __init__(self, driver):
        self.driver = driver
        self.today = date.today()
        self.date_hour = datetime.datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")
        self.article_with_Video = r"./Tvn_24_2021/Report/Allure/allure-results/Screenshots"
        self.article_without_Video = "Report/Allure/allure-results/Screenshots"

    def article_video_presence_check(self):
        driver = self.driver
        screenshot_name = f'Article_with_video_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.article_with_Video}/{screenshot_name}')
        print(f"Screenshot saved as {screenshot_name}")
        print(self.date_hour)
