import pytest
import allure
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait

from Main_page_articles.Open_Tvn24_with_rodo_accept import Open_Tvn24_with_rodo_accept
from Main_page_articles.Articles_first_FIVE_links import first_five_articles_and_urgent
from Video.Video_check import Video_check


@pytest.mark.usefixtures("local_setup")
# @pytest.mark.usefixtures("driver_chrome")
@allure.title("Tvn24 - Test obecnosci video w reportażach")
class Test_Articles:

    @allure.description("Test na obecność Video w 5 artykułach dnia")
    def test_News_Articles_Video_Presence(self):
        Tvn24_page_open = Open_Tvn24_with_rodo_accept(self.driver)
        Articles = first_five_articles_and_urgent(self.driver)
        Video = Video_check(self.driver)

        Tvn24_page_open.open_tvn24_page()
        Tvn24_page_open.onetrust_rodo_accept()
        Tvn24_page_open.tvn24_load_page_check()

        news_of_the_day = Articles.news_of_the_day_links()
        counter = 1
        for article in news_of_the_day:
            Pass_Url = "tvn24.pl/go"
            if Pass_Url not in article:
                print(f"Open Link nr. {counter} :")
                print(article)
                Video.open_page_from_href(article)
                Video.article_rodo_accept()
                Video.Video_presence_all()
                counter += 1

            else:
                print(f"Open Link nr. {counter} :")
                print(article)
                print(f"Article without Video")
                counter += 1
                continue
