from datetime import datetime

import pytest
import allure
from allure_commons.types import AttachmentType
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait

from Main_page_articles.Open_Tvn24_with_rodo_accept import Open_Tvn24_with_rodo_accept as Open
from Main_page_articles.Articles_first_FIVE_links import first_five_articles_and_urgent
from Video.Video_check import Video_check


@pytest.mark.usefixtures("local_setup")
# @pytest.mark.usefixtures("driver_chrome")
@allure.title("Tvn24 - Test obecnosci video w reportażach")
class Test_Articles:


    # @allure.description("Test na obecność Video w 5 najnowszych artykułach")
    # def test_Main_Articles_Video_Presence(self):
    #     Tvn24_page_open = Open(self.driver)
    #     Articles = first_five_articles_and_urgent(self.driver)
    #     Video = Video_check(self.driver)
    #
    #     Tvn24_page_open.open_tvn24_page()
    #     Tvn24_page_open.onetrust_rodo_accept()
    #     Tvn24_page_open.tvn24_load_page_check()
    #
    #     Wide_column_articles_list = Articles.articles_links()
    #     counter = 1
    #     for article in Wide_column_articles_list:
    #         Pass_Url = "tvn24.pl/go"
    #         if Pass_Url not in article:
    #             print(f"Open Link nr. {counter} :")
    #             print(article)
    #             Video.open_page_from_href(article)
    #             Video.article_rodo_accept()
    #             Video.Video_presence_all()
    #             counter += 1
    #         else:
    #             print(f"Open Link nr. {counter} :")
    #             print(article)
    #             print(f"Article without Video")
    #             counter += 1
    #             continue


    def test_button_Articles_Video_Presence(self):
        Tvn24_page_open = Open(self.driver)
        # Articles = first_five_articles_and_urgent(self.driver)
        # Video = Video_check(self.driver)

        Tvn24_page_open.open_tvn24_page()
        Tvn24_page_open.onetrust_rodo_accept()
        Tvn24_page_open.tvn24_load_page_check()

        self.date_hour = datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")
        self.CSS_header = "li.header__list-item--menu"
        self.CSS_header_categories_visible = "header .header-menu__row-list>li"

        self.category_button_class = "header-menu__item"
        self.category_roll_button_class = "header-menu__item header-menu__item--with-submenu"

        self.hidden_button_class_name = "not (contains(@class, 'header-menu__item--hidden'))"
        self.more_button_xpath = f".//header//button[@class='header-menu__more-button']"
        driver = self.driver
        category = "NAJNOWSZE"
        header = driver.find_element(By.CSS_SELECTOR, self.CSS_header)
        driver.execute_script("arguments[0].scrollIntoView(true);", header)

        menu_categories_visible = driver.find_elements(By.CSS_SELECTOR, self.CSS_header_categories_visible)
        print("found: ", len(menu_categories_visible))
        for category_button in menu_categories_visible:
            category_button_class = category_button.get_attribute("class")
            if category_button_class == self.category_button_class and category_button_class != self.category_roll_button_class:
                category_button_name = category_button.find_element_by_tag_name("a")
                if category_button_name.get_attribute("innerText") == category:
                    print(f"User click on {category} button")
                    AC(driver).move_to_element(category_button_name).perform()
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name=f"User click on {category} button - {self.date_hour}",
                                  attachment_type=AttachmentType.PNG)
                    category_button_name.click()
                else:
                    continue
            else:
                continue


