import pytest
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import allure
import os


@pytest.fixture()
def local_setup(request):
    options = Options()
    options.page_load_strategy = 'normal'
    chrome_options = webdriver.ChromeOptions()

    if os.name == 'nt':
        # chrome_options.add_argument('window-size=1920x1080')
        chrome_options.add_argument('start-maximized')
        driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        # driver = webdriver.Remote(
        #     "https://gil.lukasz:2c113938-6436-4bd4-945b-1f7e5ecec48c@ondemand.eu-central-1.saucelabs.com:443/wd/hub")
        request.cls.driver = driver
        # driver.maximize_window()

    else:
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        # chrome_options.add_argument('start-maximized')
        # chrome_options.add_argument("window-size=1920,1080")
        driver = webdriver.Chrome(options=chrome_options)
        request.cls.driver = driver

    yield
    driver.quit()


@pytest.fixture
def saucelab_driver_chrome(request):
    sauce_username = "gil.lukasz"
    sauce_access_key = "374d1f14-c4ad-42ff-9772-22a5564a4500"
    remote_url = f"https://{sauce_username}:{sauce_access_key}@ondemand.eu-central-1.saucelabs.com:443/wd/hub"
    test_name = request.node.name

    options = Options()
    options.page_load_strategy = 'normal'

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('start-maximized')

    sauceOptions = {
        "screenResolution": "1920x1080",
        "platformName": "Windows 10",
        "browserVersion": "latest",
        "seleniumVersion": "3.11.0",
        'name': test_name
    }

    chrome_options = {
        "browserVersion": "latest",
        'platformName': "Windows 10",
        'browserName': "chrome",
        'sauce:options': sauceOptions

    }

    driver = webdriver.Remote(command_executor=remote_url, desired_capabilities=chrome_options, options=options)
    request.cls.driver = driver

    if driver is not None:
        print("SauceOnDemandSessionID={} job-name={}".format(driver.session_id, test_name))
    else:
        raise WebDriverException("Never created!")

    yield
    driver.quit()


@pytest.fixture
def saucelab_driver_firefox(request):
    sauce_username = "gil.lukasz"
    sauce_access_key = "374d1f14-c4ad-42ff-9772-22a5564a4500"
    remote_url = f"https://{sauce_username}:{sauce_access_key}@ondemand.eu-central-1.saucelabs.com:443/wd/hub"
    test_name = request.node.name

    options = Options()
    options.page_load_strategy = 'normal'

    firefox_options = webdriver.FirefoxOptions
    firefox_options.add_argument('start-maximized')

    sauceOptions = {
        "screenResolution": "1920x1080",
        "platformName": "Windows 10",
        "browserVersion": "latest",
        "seleniumVersion": "3.11.0",
        'name': test_name
    }

    firefox_options = {
        "browserVersion": "latest",
        'platformName': "Windows 10",
        'browserName': "firefox",
        'sauce:options': sauceOptions

    }

    driver = webdriver.Remote(command_executor=remote_url, desired_capabilities=firefox_options, options=options)
    request.cls.driver = driver

    if driver is not None:
        print("SauceOnDemandSessionID={} job-name={}".format(driver.session_id, test_name))
    else:
        raise WebDriverException("Never created!")
    yield
    driver.quit()
