import pytest
import allure
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait

from Main_page_articles.Open_Tvn24_with_rodo_accept import Open_Tvn24_with_rodo_accept
from Main_page_articles.Articles_first_FIVE_links import first_five_articles_and_urgent
from Video.Video_check import Video_check


@pytest.mark.usefixtures("local_setup")
# @pytest.mark.usefixtures("driver_chrome")
@allure.title("Tvn24 - Test obecnosci video w reportażach")
class Test_Articles:

    @allure.description("Test na obecność Video w artykule - TOP")
    def test_Urgent_Article_Video_Presence(self):
        Tvn24_page_open = Open_Tvn24_with_rodo_accept(self.driver)
        Article = first_five_articles_and_urgent(self.driver)
        Video = Video_check(self.driver)

        Tvn24_page_open.open_tvn24_page()
        Tvn24_page_open.onetrust_rodo_accept()
        Tvn24_page_open.tvn24_load_page_check()
        Top_article = Article.top_urgent_article_link()

        Pass_Url = "tvn24.pl/go"

        if Pass_Url not in Top_article:
            print(f"Open Link:")
            print(Top_article)
            Video.open_page_from_href(Top_article)
            Video.article_rodo_accept()
            Video.Video_presence_all()
        else:
            print(f"Open Link:")
            print(Top_article)
            print(f"Article without Video")

