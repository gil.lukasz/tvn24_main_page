from Account_page.Move_to_account_page import account_page
import pytest
import allure

from Account_page.Facebook_login import Facebook_login_Passed
from Account_page.Facebook_login import Facebook_login_Password_Fail
from Account_page.Facebook_login import Facebook_login_Username_Fail
from Account_page.Move_to_account_page import account_page
from Account_page.Test_Screenshots import Account_Screenshots
from Account_page.method_login import method_login


@pytest.mark.usefixtures("setup")
@allure.title("Tvn24 - Logowanie do konta")
class Test_Log_in:

    @allure.description("Test logowania za pomocą Facebook - Poprawne Dane")
    def test_Facebook_login_method_Passed(self):
        Account_page = account_page(self.driver)
        Login_method = method_login(self.driver)
        Facebook_login = Facebook_login_Passed(self.driver)

        Screenshot = Account_Screenshots(self.driver)

        while True:
            Account_page.open_tvn24_page()
            Account_page.rodo_accept()
            Account_page.move_to_account_page()
            Login_method.facebook_method()
            Facebook_login.cookie_accept()
            Facebook_login.log_in()
            # Facebook_login.login_passed_check()
            # Screenshot.facebook_user_log_in_passed_screenshot()
            break

    # @allure.description("Logowanie By Facebook - Nieprawidłowy UserName")
    # def test_Facebook_login_method_Username_Fail(self):
    #     Account_page = account_page(self.driver)
    #     Login_method = method_login(self.driver)
    #     Facebook_login = Facebook_login_Username_Fail(self.driver)
    #
    #     Account_page.open_tvn24_page()
    #     Account_page.rodo_accept()
    #     Account_page.move_to_account_page()
    #     Login_method.facebook_method()
    #     Facebook_login.cookie_accept()
    #     Facebook_login.log_in()
    #     Facebook_login.log_in_passed_check()
    #
    # @allure.description("Logowanie By Facebook - Nieprawidłowe Password")
    # def test_Facebook_login_method_Password_Fail(self):
    #     Account_page = account_page(self.driver)
    #     Login_method = method_login(self.driver)
    #     Facebook_login = Facebook_login_Password_Fail(self.driver)
    #
    #     Account_page.open_tvn24_page()
    #     Account_page.rodo_accept()
    #     Account_page.move_to_account_page()
    #     Login_method.facebook_method()
    #     Facebook_login.cookie_accept()
    #     Facebook_login.log_in()
    #     Facebook_login.log_in_passed_check()
