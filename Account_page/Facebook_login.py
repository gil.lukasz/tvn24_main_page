import allure
from allure_commons.types import AttachmentType
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait

from Account_page.Test_Screenshots import Account_Screenshots as Screenshot

@allure.description("Logowanie by FB - Powodzenie")
class Facebook_login_Passed:

    def __init__(self, driver):
        self.driver = driver

        self.cookies_alert_accept = (By.CSS_SELECTOR, 'button[data-cookiebanner="accept_button"]')
        self.user_name_input = "email"
        self.password_input = "pass"
        self.login_button = "loginbutton"
        self.account_authorized = (By.CSS_SELECTOR, "div.account-standard--authorized")
        self.expected_authorized_class = "account-standard account-standard--authorized"
        self.expected_url_after_login = "tvn24.pl"
        self.username = "gil.lukasz@wp.pl"
        self.password = "Vegeta11081984"

    @allure.step("Oczekiwanie i akceptacja ewentualnych Cookies")
    def cookie_accept(self):
        driver = self.driver
        attempt = 1
        for approach in range(5):
            try:
                accept_button = Wait(driver, 10, 1).until(EC.presence_of_element_located(self.cookies_alert_accept))
                Screenshot(driver).facebook_unexpected_error_screenshot()
                accept_button.click()
                print("Cookie alert appears")
                break
            except:
                attempt += 1
                continue
        else:
            print("No cookie alert appears")

    @allure.step("Uzupełnienie pól - Username i Password")
    def log_in(self):

        driver = self.driver

        driver.find_element(By.ID, self.user_name_input).click()
        driver.find_element(By.ID, self.user_name_input).send_keys(self.username)
        Screenshot(driver).facebook_unexpected_error_screenshot()
        driver.find_element(By.ID, self.password_input).click()
        driver.find_element(By.ID, self.password_input).send_keys(self.password)
        allure.attach(self.driver.get_screenshot_as_png(),
                      name="Logowanie By FB - Dane Input",
                      attachment_type=AttachmentType.PNG)
        login_button = driver.find_element(By.ID, self.login_button)
        login_button.click()

    @allure.step("Weryfikacja zalogowania się")
    def login_passed_check(self):
        driver = self.driver
        attempt = 1
        for approach in range(5):
            try:
                Wait(driver, 10, 1).until(EC.url_contains(self.expected_url_after_login))
                account_button = Wait(driver, 10, 1).until(EC.presence_of_element_located(self.account_authorized))
                if self.expected_authorized_class == account_button.get_attribute("class"):
                    print(f"Logowanie za pomocą 'Facebook' - Udane ")
                    break
                else:
                    continue
            except:
                print(f"Logowanie za pomocą 'Facebook' - Nieudane")
                attempt += 1
                continue
        allure.attach(self.driver.get_screenshot_as_png(),
                      name="Logowanie zakonczone powodzeniem",
                      attachment_type=AttachmentType.PNG)


@allure.description("Logowanie by FB - Błędny USER")
class Facebook_login_Username_Fail:

    def __init__(self, driver):
        self.driver = driver
        self.cookies_alert_accept = (By.CSS_SELECTOR, 'button[data-cookiebanner="accept_button"]')
        self.user_name_input = "email"
        self.password_input = "pass"
        self.login_button = "loginbutton"
        self.account_authorized = (By.CSS_SELECTOR, "div.account-standard--authorized")
        self.expected_authorized_class = "account-standard account-standard--authorized"
        self.expected_url_after_login = "tvn24.pl"
        self.username = "gil.lukasz111@wp.pl"
        self.password = "Vegeta11081984"
        self.user_value_error = (By.CSS_SELECTOR, "div[role='alert']")

    @allure.step("Oczekiwanie i akceptacja ewentualnych Cookies")
    def cookie_accept(self):
        driver = self.driver
        attempt = 0
        while attempt <= 3:
            try:
                accept_button = Wait(driver, 2, 0.5).until(
                    EC.element_to_be_clickable(self.cookies_alert_accept))
                accept_button.click()
                break
            except:
                attempt += 1
                continue
        else:
            print("No cookie alert appears")

    @allure.step("Uzupełnienie pól - Username i Password")
    def log_in(self):

        driver = self.driver
        driver.find_element(By.ID, self.user_name_input).click()
        driver.find_element(By.ID, self.user_name_input).send_keys(self.username)
        driver.find_element(By.ID, self.password_input).click()
        driver.find_element(By.ID, self.password_input).send_keys(self.password)
        allure.attach(self.driver.get_screenshot_as_png(),
                      name="Logowanie By FB - Dane Input",
                      attachment_type=AttachmentType.PNG)
        login_button = driver.find_element(By.ID, self.login_button)
        login_button.click()

    @allure.step("Weryfikacja zalogowania się")
    def log_in_passed_check(self):
        driver = self.driver
        attempt = 1
        while attempt <= 2:
            try:
                Wait(driver, 10, 1).until(EC.presence_of_element_located(self.user_value_error))
                allure.attach(self.driver.get_screenshot_as_png(),
                              name="Logowanie By FB - Błędny USERNAME",
                              attachment_type=AttachmentType.PNG)
                print(f"Nieprawidłowy USERNAME - Wyświetlono Alert ")
                break
            except:
                attempt += 1
                continue


@allure.description("Logowanie by FB - Błędne PASSWORD")
class Facebook_login_Password_Fail:

    def __init__(self, driver):
        self.driver = driver

        self.cookies_alert_accept = (By.CSS_SELECTOR, 'button[data-cookiebanner="accept_button"]')
        self.user_name_input = "email"
        self.password_input = "pass"
        self.login_button = "loginbutton"
        self.account_authorized = (By.CSS_SELECTOR, "div.account-standard--authorized")
        self.expected_authorized_class = "account-standard account-standard--authorized"
        self.expected_url_after_login = "tvn24.pl"
        self.username = "gil.lukasz@wp.pl"
        self.password = "Vegeta"
        self.user_value_error = (By.CSS_SELECTOR, "div[role='alert']")

    @allure.step("Oczekiwanie i akceptacja ewentualnych Cookies")
    def cookie_accept(self):
        driver = self.driver
        attempt = 0
        while attempt <= 3:
            try:
                accept_button = Wait(driver, 2, 0.5).until(
                    EC.element_to_be_clickable(self.cookies_alert_accept))
                accept_button.click()
                break
            except:
                attempt += 1
                continue
        else:
            print("No cookie alert appears")

    @allure.step("Uzupełnienie pól - Username i Password")
    def log_in(self):

        driver = self.driver
        driver.find_element(By.ID, self.user_name_input).click()
        driver.find_element(By.ID, self.user_name_input).send_keys(self.username)
        driver.find_element(By.ID, self.password_input).click()
        driver.find_element(By.ID, self.password_input).send_keys(self.password)
        allure.attach(self.driver.get_screenshot_as_png(),
                      name="Logowanie By FB - Dane Input",
                      attachment_type=AttachmentType.PNG)
        login_button = driver.find_element(By.ID, self.login_button)
        login_button.click()

    @allure.step("Weryfikacja zalogowania się")
    def log_in_passed_check(self):
        # Screenshot = Account_Screenshots(self.driver)
        driver = self.driver

        attempt = 1
        while attempt <= 2:
            try:
                Wait(driver, 10, 1).until(EC.presence_of_element_located(self.user_value_error))
                allure.attach(self.driver.get_screenshot_as_png(),
                              name="Logowanie By FB - Błędne PASSWORD",
                              attachment_type=AttachmentType.PNG)
                print(f"Nieprawidłowe PASSWORD - Wyświetlono Alert ")
                break
            except:
                attempt += 1
                continue
