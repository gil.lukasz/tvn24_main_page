from allure_commons.types import AttachmentType
from selenium.common.exceptions import NoSuchElementException
import allure
import pytest
import pytest_selenium
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from Account_page.Test_Screenshots import Account_Screenshots as Screenshot


@allure.description("Przejscie na stronę logowania do konta użytkownika")
class account_page:

    def __init__(self, driver):

        self.driver = driver
        self.cookie_accept = (By.ID, "onetrust-accept-btn-handler")
        self.rodo_accept_button = "onetrust-accept-btn-handler"
        self.account = (By.CSS_SELECTOR, "li.header__list-item--account")
        self.button_logIn = (By.CSS_SELECTOR,
                             "div.account-standard__popup button.account-content__button.account-content__button--large")
        self.account_authorized = (By.CSS_SELECTOR, "div.account-standard--authorized")
        self.expected_url = "account.tvn.pl"

    @allure.step("Otwarcie strony Tvn24.pl")
    def open_tvn24_page(self):
        driver = self.driver
        driver.get("https://tvn24.pl/")

    @allure.step("Akceptacja i czekanie na Rodo")
    def rodo_accept(self):
        driver = self.driver
        Max_time_to_wait = 5
        try:
            cookie_accept = Wait(driver, Max_time_to_wait, 0.5).until(
                EC.presence_of_element_located((By.ID, self.rodo_accept_button)))
            cookie_accept.click()
            Wait(driver, 5, 1).until_not(EC.element_to_be_clickable((By.ID, self.rodo_accept_button)))
            print(f"On page {driver.current_url} RODO accept button appeared")
        except:
            print(f"On page {driver.current_url} no RODO appeared in {Max_time_to_wait} seconds ")

    @allure.step("Akceptacja i czekanie na Rodo")
    def cookie_alert(self):
        driver = self.driver

        attempt = 0
        while attempt < 3:
            try:
                cookie_accept = Wait(driver, 2, 0.5).until(EC.element_to_be_clickable(self.cookie_accept))
                cookie_accept.click()
                break
            except NoSuchElementException:
                attempt += 1
                continue
        else:
            print(f"On page {driver.current_url} no alert appeared ")

    @allure.step("Przejscie do strony konta użytkownika")
    def move_to_account_page(self):
        driver = self.driver
        attempt = 1
        for approach in range(5):
            try:
                account_button = Wait(driver, 5, 1).until(EC.presence_of_element_located(self.account))
                AC(driver).move_to_element(account_button).click(account_button).perform()
                allure.attach(self.driver.get_screenshot_as_png(),
                              name="Przejscie do strony konta użytkownika",
                              attachment_type=AttachmentType.PNG)
                LogIn_button = Wait(driver, 5, 1).until(EC.visibility_of_element_located(self.button_logIn))
                LogIn_button.click()
                Wait(driver, 10, 1).until(EC.url_contains(self.expected_url))
                Screenshot(driver).log_in_method_page()
                print(f"Klikniecie w 'Zaloguj' - Próba nr {attempt} Udana ")
                break

            except:
                print(f"Klikniecie w 'Zaloguj' - Próba nr {attempt} Nieudana ")
                attempt += 1
                continue
        else:
            return False

    # def move_to_account_page(self):
    #     driver = self.driver
    #     attempt = 1
    #     while attempt < 10:
    #         try:
    #             account_button = Wait(driver, 5, 1).until(EC.presence_of_element_located(self.account))
    #             AC(driver).move_to_element(account_button).perform()
    #             allure.attach(self.driver.get_screenshot_as_png(),
    #                           name="Przejscie do strony konta użytkownika",
    #                           attachment_type=AttachmentType.PNG)
    #             LogIn_button = Wait(driver, 5, 1).until(EC.visibility_of_element_located(self.button_logIn))
    #             LogIn_button.click()
    #             # Wait(driver, 10, 1).until(EC.url_contains(self.expected_url))
    #             assert self.expected_url in driver.current_url
    #             Screenshot(driver).log_in_method_page()
    #             print(f"Klikniecie w 'Zaloguj' - Próba nr {attempt} Udana ")
    #             break
    #         except:
    #             print(f"Klikniecie w 'Zaloguj' - Próba nr {attempt} Nieudana ")
    #             attempt += 1
    #             continue
