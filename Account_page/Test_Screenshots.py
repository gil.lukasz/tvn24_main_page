import time
from datetime import date, datetime
import datetime


class Account_Screenshots:

    def __init__(self, driver):
        self.driver = driver
        self.today = date.today()
        self.date_hour = datetime.datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")
        self.screenshot_Errors_path = r"D:\Lukasz\Tvn_24_2021\Tvn24_test_results\Screenshots\Account_tests\FB_failed"
        self.screenshot_Passed_path = r"D:\Lukasz\Tvn_24_2021\Tvn24_test_results\Screenshots\Account_tests\FB_Pass"
        self.screenshot_FP_page = r"D:\Lukasz\Tvn_24_2021\Tvn24_test_results\Screenshots\Account_tests\FB_page"
        self.screenshot_login_method_page = r"D:\Lukasz\Tvn_24_2021\Tvn24_test_results\Screenshots\Account_tests\Login_method_page"

    def log_in_method_page(self):
        driver = self.driver
        screenshot_name = f'login_method_page_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.screenshot_login_method_page}\{screenshot_name}')
        print(f"Screenshot saved as {screenshot_name}")
        print(self.date_hour)

    def log_in_FB_page(self):
        driver = self.driver
        screenshot_name = f'login_method_page_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.screenshot_FP_page}\{screenshot_name}')
        print(f"Screenshot saved as {screenshot_name}")
        print(self.date_hour)

    def facebook_user_log_in_passed_screenshot(self):
        driver = self.driver
        screenshot_name = f'FB_login_Passed_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.screenshot_Passed_path}\{screenshot_name}')
        print(f"Screenshot saved as {screenshot_name}")
        print(self.date_hour)

    def facebook_user_value_error_screenshot(self):
        driver = self.driver
        screenshot_name = f'FB_user_value_error_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.screenshot_Errors_path}\User_value_error\{screenshot_name}')
        print(f"Screenshot saved as {screenshot_name}")

    def facebook_password_value_error_screenshot(self):
        driver = self.driver
        screenshot_name = f'FB_password_value_error_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.screenshot_Errors_path}\Password_value_error\{screenshot_name}')
        print(f"Screenshot saved as {screenshot_name}")

    def facebook_unexpected_error_screenshot(self):
        driver = self.driver
        screenshot_name = f'FB_Unexpected_error_{self.date_hour}.png'
        driver.get_screenshot_as_file(rf'{self.screenshot_Errors_path}\Unexpected_error\{screenshot_name}')
        print(f"Screenshot saved as {screenshot_name}")
