import allure

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains as AC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from Account_page.Test_Screenshots import Account_Screenshots as Screenshot


@allure.description("Wybór metody logowania")
class method_login:

    def __init__(self, driver):

        self.driver = driver
        self.Mail = (By.ID, "login_by_email")
        self.Facebook = (By.ID, "login_by_fb")
        self.expected_facebook_url = "facebook.com"
        self.Google = (By.ID, "login_by_g")
        self.Twitter = (By.ID, "login_by_tw")
        self.Apple = (By.ID, "login_by_apple")

    @allure.step("Wybór metody logowania - Facebook")
    def facebook_method(self):

        driver = self.driver
        attempt = 1
        for approach in range(5):

            try:
                Facebook_button = Wait(driver, 5, 1).until(EC.element_to_be_clickable(self.Facebook))
                AC(driver).move_to_element(Facebook_button).click(Facebook_button).perform()
                # assert self.expected_facebook_url in driver.current_url
                # Wait(driver, 10, 1).until(EC.url_contains(self.expected_facebook_url))
                print(f"Klikniecie w przycisk 'Zaloguj się przez Facebook' - Próba nr {attempt} Udana ")
                break
            except:
                print(f"Klikniecie w przycisk 'Zaloguj się przez Facebook' - Próba nr {attempt} nieudana ")
                attempt += 1
                continue
        else:
            return False
