import time

import allure
from allure_commons.types import AttachmentType
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.common.exceptions import TimeoutException

from Base_Class.Test_Screenshots_methods import Screenshots

from datetime import date, datetime
import datetime


class Open_Tvn24_with_rodo_accept:

    def __init__(self, driver):
        self.driver = driver
        self.date_hour = datetime.datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")

        self.rodo_accept = (By.ID, "onetrust-accept-btn-handler")
        self.urgent_article = (By.ID, 'urgentStandard')

    @allure.step("Otwarcie strony Tvn24.pl")
    def open_tvn24_page(self):
        driver = self.driver
        driver.get("https://tvn24.pl/")

    @allure.step("Akceptacja i czekanie na Rodo")
    def onetrust_rodo_accept(self):
        driver = self.driver
        attempt = 1
        for approach in range(3):
            try:
                accept_button = Wait(driver, 3, 0.5).until(EC.presence_of_element_located(self.rodo_accept))
                # driver.execute_script("return arguments[0].scrollIntoView(true);", accept_button)
                # Screenshots(self.driver).rodo_main_page_screenshot()

                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Rodo_alert - {self.date_hour}",
                              attachment_type=AttachmentType.PNG)
                accept_button.click()

                print(f'Rodo Approval - after {attempt} attempt')
                break
            except:
                print(f'Rodo alert not found - after {attempt} attempt')
                attempt += 1
                continue
        else:
            print('Rodo alert - was not displayed')

    @allure.step("Weryfikacja załadowania strony tvn24.pl")
    def tvn24_load_page_check(self):
        driver = self.driver

        attempt = 1
        for approach in range(5):
            try:
                Wait(driver, 5, 1).until(EC.presence_of_element_located(self.urgent_article))
                # menu_header = Wait(driver, 5, 1).until(EC.presence_of_element_located(self.urgent_article))
                # driver.execute_script("return arguments[0].scrollIntoView(true);", menu_header)
                time.sleep(2)
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Main page Tvn24 - {self.date_hour}",
                              attachment_type=AttachmentType.PNG)
                print('Main page Tvn24 - load success')
                break
            except:
                attempt += 1
                continue
        else:
            print('Main page Tvn24 - load Fail')
