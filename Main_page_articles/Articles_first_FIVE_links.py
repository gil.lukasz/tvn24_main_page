import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait

from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException


class first_five_articles_and_urgent:

    def __init__(self, driver):
        self.driver = driver

        self.urgent_article_link = '#urgentStandard a[role]'

        self.first_section = 'section.two-column:first-of-type'

        self.first_section_wide_column_articles = 'section.two-column:first-of-type div.wide-column article'
        self.wide_column_articles_href_value = "article a"

        self.first_section_narrow_column_articles = 'section.two-column:first-of-type section:first-of-type ul li.news-of-the-day__item'
        self.narrow_column_articles_href_value = "a"

    @allure.step("Reportaż Główny - Pobranie linku ")
    def top_urgent_article_link(self):
        driver = self.driver
        urgent_article = Wait(driver, 10, 1).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, self.urgent_article_link)))
        urgent_article_href = urgent_article.get_attribute("href")
        return urgent_article_href

    @allure.step("Reportaże - Pobranie pierwszych 5 linków ")
    def articles_links(self):
        driver = self.driver
        articles_href_list = []

        Wait(driver, 5, 0.5).until(EC.presence_of_all_elements_located((
            By.CSS_SELECTOR, self.first_section)))

        articles = driver.find_elements(By.CSS_SELECTOR, self.first_section_wide_column_articles)
        print(f"Articles found in wide column - {len(articles)}")

        counter = 1
        for article in articles:
            if counter < 6:
                try:
                    print(f"Getting attribute 'Href' from tag article no. {counter}")
                    article_href = article.find_element(
                        By.CSS_SELECTOR, self.wide_column_articles_href_value).get_attribute("href")
                    print(f"'Href' no. {counter} - {article_href}")
                    articles_href_list.append(article_href)
                    counter += 1
                except NoSuchElementException:
                    print(f"NoSuchElementException : No link was found in article no. {counter}")
                except NoSuchAttributeException:
                    print(f"NoSuchAttributeException : No link was found in article no. {counter}")
                continue
            else:
                break

        print(f"Articles left in articles href list - {len(articles_href_list)}")
        return articles_href_list

    @allure.step("Wiadomosci dnia - Pobranie pierwszych 5 linków ")
    def news_of_the_day_links(self):
        driver = self.driver
        Information_of_the_day_href_list = []

        Wait(driver, 5, 0.5).until(EC.presence_of_all_elements_located((
            By.CSS_SELECTOR, self.first_section)))

        Information_of_the_day = driver.find_elements(
            By.CSS_SELECTOR, self.first_section_narrow_column_articles)
        print(f"Articles found in Information of the day  - {len(Information_of_the_day)}")

        counter = 1
        for news in Information_of_the_day:
            if counter < 6:
                try:
                    print(f"Getting attribute 'Href' from tag article no. {counter}")
                    article_href = news.find_element(
                        By.CSS_SELECTOR, self.narrow_column_articles_href_value).get_attribute("href")
                    print(article_href)
                    Information_of_the_day_href_list.append(article_href)
                    counter += 1
                except NoSuchElementException:
                    print(f"No link was found in article no. {counter}")
                continue
            else:
                break

        print(f"Articles left in the Information of the day href list - {len(Information_of_the_day_href_list)}")
        return Information_of_the_day_href_list
