import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver import ActionChains as ac
from selenium.webdriver.support import select as se
from selenium.webdriver.common.keys import Keys

from Helpers import Element_View_Status_Check as ES
from Tvn24_locators import Tvn24_locators as locators
from selenium.common.exceptions import NoSuchElementException


def alert_accept_try(driver, timeout, poll_frequency):
    test = False
    try:
        locator = (By.ID, 'onetrust-accept-btn-handler')
        wait = WebDriverWait(driver, timeout, poll_frequency)
        accept_button = wait.until(ec.visibility_of_element_located(locator))
        accept_button.click()

        test = True
    except:
        print("")

    if test == False:
        try:
            locator = (By.XPATH, './/*[@id="rodoLayer"]//a[@class="rodoFooterBtnAccept"]')
            wait = WebDriverWait(driver, timeout, poll_frequency)
            accept_button = wait.until(ec.visibility_of_element_located(locator))
            accept_button.click()
        except:
            print(f"Any alert appeared on page {driver.current_url}")


def alert_accept_while_if_method(driver):
    seconds = 0
    while seconds < 3:
        V1 = driver.find_elements_by_xpath('.//*[@id="onetrust-accept-btn-handler"]')
        V2 = driver.find_elements_by_xpath('.//*[@id="rodoLayer"]//a[@class="rodoFooterBtnAccept"]')
        if len(V1) > 0:
            V1[0].click()
            print(f"On page {driver.current_url} onetrust alert accepted")
            return False
        elif len(V2):
            V2[0].click()
            print(f"On page {driver.current_url} old alert appeared")
            return False
        else:
            seconds += 1
            time.sleep(1)
    else:
        print(f"On page {driver.current_url} no alert appeared ")
        return False


def displayed_buttons_txt_list(driver):
    xpath = ".//*[@id='__next']//header//ul[@class ='header-menu__row-list']//a"
    displayed_buttons_list = []

    buttons_list = driver.find_elements(By.XPATH, xpath)
    for button in buttons_list:
        if button.is_displayed():
            Displayed_Menu_Button_Link = button.get_attribute("href")
            Displayed_Menu_Button_Text = button.text
            displayed_buttons_list.append(Displayed_Menu_Button_Link)
            print(f"On page {driver.current_url} visible button '{Displayed_Menu_Button_Text}' forward to Url - "
                  f"{Displayed_Menu_Button_Link} ")

    print("Visible buttons found : ", len(displayed_buttons_list))
    print(*displayed_buttons_list, sep='\n')
    return displayed_buttons_list


def not_displayed_buttons_txt_list(driver):
    xpath_hidden_list_V1 = ".//*[@id='__next']//header//ul[@class ='header-menu__more-list-elements']//a"
    xpath_hidden_list_V2 = ".//*[@id='__next']//header//ul[contains(@class,'header-menu__item') " \
                           "and not (contains(@class, 'header-menu__item--hidden'))]//a"
    hidden_buttons_list = []

    # Wait = WebDriverWait(driver, timeout=5, poll_frequency=0.5)
    # Wait.until(EC.visibility_of_any_elements_located(xpath))

    buttons_list = driver.find_elements(By.XPATH, xpath_hidden_list_V1)
    for button in buttons_list:
        # if button.is_displayed():
        #     pass
        # else:
        Hidden_Menu_Button_Link = button.get_attribute("href")
        Displayed_Menu_Button_Text = button.get_attribute('textContent')
        hidden_buttons_list.append(Hidden_Menu_Button_Link)
        # print(f"On page {driver.current_url}  hidden button '{Displayed_Menu_Button_Text}' forward to Url - "
        #       f"{Hidden_Menu_Button_Link} ")

    print(f"Hidden buttons found : ", len(hidden_buttons_list))
    print(*hidden_buttons_list, sep='\n')
    return hidden_buttons_list


def hover_method_v1(driver):
    displayed_header_menu_xpah = "ul[@class ='header-menu__row-list']"
    hidden_header_menu_xpath = "ul[@class='header-menu__more-list-elements']"

    single_button_class_name = "header-menu__item"
    roll_button_class_name = "header-menu__item header-menu__item--with-submenu"
    hidden_button_class_name = "not (contains(@class, 'header-menu__item--hidden'))"
    more_button_xpath = f".//header//button[@class='header-menu__more-button']"

    scroll_to_the_top = driver.find_element_by_xpath(".//header/nav")
    driver.execute_script("arguments[0].scrollIntoView({behavior: 'smooth'});", scroll_to_the_top)

    displayed_header_buttons = driver.find_elements_by_xpath(
        f".//header//{displayed_header_menu_xpah}/li[{hidden_button_class_name}]")
    print(len(displayed_header_buttons))

    more_button = driver.find_element_by_xpath(more_button_xpath)

    for button in displayed_header_buttons:
        button_class_name = button.get_attribute("class")
        if button_class_name == single_button_class_name and button_class_name != roll_button_class_name:
            button_link = button.find_element_by_tag_name("a")
            print(button_link.text)
            ac(driver).move_to_element(button_link).perform()
            time.sleep(0.5)
        elif button_class_name == roll_button_class_name:
            driver.execute_script("arguments[0].scrollIntoView({behavior: 'smooth'});", scroll_to_the_top)
            button_link = button.find_element_by_tag_name("a")
            ac(driver).move_to_element(button_link).perform()
            time.sleep(0.1)
            inner_buttons_list = button.find_elements_by_tag_name("li")
            for inner_button in inner_buttons_list:
                inner_button_class_name = inner_button.get_attribute("class")
                inner_li_sub_menu_class_name_v1 = "header-menu__item"
                inner_li_sub_menu_class_name_v2 = "header-menu__item header-menu__item--active"
                if inner_button_class_name == inner_li_sub_menu_class_name_v1 or inner_li_sub_menu_class_name_v2:
                    inner_button_link = inner_button.find_element_by_tag_name("a")
                    print(inner_button_link.text)
                    ac(driver).move_to_element(inner_button_link).perform()
                    time.sleep(0.5)
                else:
                    continue

    ac(driver).move_to_element(more_button).perform()
    time.sleep(1)

    hidden_header_buttons = driver.find_elements_by_xpath(
        f".//header//{hidden_header_menu_xpath}/li")
    print(len(hidden_header_buttons))

    for button in hidden_header_buttons:
        driver.execute_script("arguments[0].scrollIntoView({behavior: 'smooth'});", scroll_to_the_top)
        button_class_name = button.get_attribute("class")
        if button_class_name == single_button_class_name:
            button_link = button.find_element_by_tag_name("a")
            print(button_link.text)
            ac(driver).move_to_element(button_link).perform()
            time.sleep(0.5)
        else:
            pass


def move_to_account_v1(driver):
    CSS_account = "button.account-standard__toggle-button"
    CSS_login = "div.account-standard__popup button.account-content__button.account-content__button--large"
    expected_url = "account.tvn.pl"

    for attempt in range(5):

        ES.element_status_checks(driver, 5, 0.5, CSS_account)
        account_button = driver.find_element_by_css_selector(CSS_account)
        driver.execute_script("return arguments[0].scrollIntoView(true);", account_button)
        ac(driver).move_to_element(account_button).click(account_button).perform()

        ES.element_status_checks(driver, 5, 0.5, CSS_login)
        login_button = driver.find_element_by_css_selector(CSS_login)
        ac(driver).move_to_element(login_button).click(login_button).perform()
        try:
            wait = WebDriverWait
            wait(driver, 10, 1).until(ec.url_contains(expected_url))
            break
        except:
            continue


def move_to_account_v2(driver):
    account = driver.find_element(locators.Tvn24_account_locator.account_frame)
    button = driver.find_element(locators.Tvn24_account_locator.account_login_button)
    expected_url = "account.tvn.pl"

    for attempt in range(5):

        ES.presence_try(driver, 5, 0.5, account)
        driver.execute_script("return arguments[0].scrollIntoView(true);", account)
        ac(driver).move_to_element(account).click(account).perform()

        ES.presence_try(driver, 5, 0.5, button)
        ac(driver).move_to_element(button).click(button).perform()
        try:
            wait = WebDriverWait
            wait(driver, 10, 1).until(ec.url_contains(expected_url))
            break
        except:
            continue


def find_articles_with_play_and_ellipseSound_buttons(driver):
    CSS_articles = "div.content-area-elements article"
    articles_main_page = driver.find_elements_by_css_selector(CSS_articles)

    CSS_articles_play_button = "div.article-play-button"
    articles_with_play_button = []

    for article in articles_main_page:
        try:
            article.find_element_by_css_selector(CSS_articles_play_button)
            articles_with_play_button.append(article)
        except NoSuchElementException:
            pass

    CSS_articles_play_href_link = "article a"
    articles_play_href_links = []

    for article in articles_with_play_button:
        article_href_link = article.find_element_by_css_selector(CSS_articles_play_href_link).get_attribute("href")
        articles_play_href_links.append(article_href_link)

    print(len(articles_play_href_links))

    return articles_play_href_links


def video_ellipseSound_big_icon_hover(driver):
    CSS_Sound_icon = "article.article__content ellipse"
    CSS_nuvi_control_bar = "div.article-story-header div[data-test-id='nuvi-control-bar']"

    for attempt in range(3):

        try:
            ES.mute_icon_presence_try(driver, 3, 0.5, CSS_Sound_icon)
            Sound_icon = driver.find_element_by_css_selector(CSS_Sound_icon)
            driver.execute_script('arguments[0].scrollIntoView({block: "center", inline: "center"})', Sound_icon)
            ac(driver).move_to_element(Sound_icon).click(Sound_icon).perform()
            nuvi_control_bar = driver.find_element_by_css_selector(CSS_nuvi_control_bar)
            driver.execute_script('arguments[0].scrollIntoView({block: "end", inline: "center"})', nuvi_control_bar)
            ES.nuvi_bar_visibility_try(driver, 3, 0.5, CSS_nuvi_control_bar)
            break
        except:
            continue


def video_nuvi_bar_hover(driver):
    CSS_nuvi_control_bar = "div.article-story-header div[data-test-id='nuvi-control-bar']"
    Css_pause_button = 'button.paused.nuviPlayPauseButton'


def login_by_mail(driver):
    wait = WebDriverWait
    Id_mail_button = "login_by_email"
    login_value = "gil.lukasz@wp.pl"
    password_value = "Gil2021!@#"

    button_after_login_xpath = "//button[@class='account-standard__toggle-button']"
    div_account_after_successful_login = "[@class='account-standard__popup']"
    expected_text_after_successful_login = "Łukasz Gil"

    login_by_Mail_button = driver.find_element_by_id(Id_mail_button)
    ac(driver).move_to_element(login_by_Mail_button).click(login_by_Mail_button).perform()

    login_input = driver.find_element_by_xpath("//*[@id='root']//input[@name='log_in']")
    password_input = driver.find_element_by_id("password")
    sign_in_button = driver.find_element_by_id("sign_in")

    ac(driver).move_to_element(login_input).click(login_input).perform()
    login_input.clear()
    login_input.send_keys(login_value)

    ac(driver).move_to_element(password_input).click(password_input).perform()
    password_input.clear()
    password_input.send_keys(password_value)

    # driver.execute_script("arguments[0].scrollIntoView({behavior: 'smooth'});", sign_in_button)
    ac(driver).move_to_element(sign_in_button).click(sign_in_button).perform()

    # time.sleep(50)

    wait(driver, 10, 1).until(
        ec.frame_to_be_available_and_switch_to_it((By.CLASS_NAME, "Z6wXJz-G-6vcu3I0ZoM_B")))
    wait(driver, 10, 1).until(
        ec.frame_to_be_available_and_switch_to_it((By.ID, "fc-iframe-wrap")))

    wait(driver, 10, 1).until(
        ec.frame_to_be_available_and_switch_to_it((By.ID, "CaptchaFrame")))
    start_button = wait(driver, 10, 1).until(
        ec.visibility_of_element_located((By.ID, 'home_children_button')))
    ac(driver).move_to_element(start_button).click(start_button).perform()
    driver.switch_to.default_content()

    wait(driver, 10, 1).until(
        ec.frame_to_be_available_and_switch_to_it((By.CLASS_NAME, "Z6wXJz-G-6vcu3I0ZoM_B")))
    wait(driver, 10, 1).until(
        ec.frame_to_be_available_and_switch_to_it((By.ID, "fc-iframe-wrap")))

    captcha_attempts = 0
    while captcha_attempts < 3:
        wait(driver, 10, 1).until(
            ec.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR, "div.ctn.front>iframe")))
        captcha_pictures = driver.find_elements_by_tag_name("a")
        for picture in captcha_pictures:
            ac(driver).move_to_element(picture).perform()
        ac(driver).move_to_element(captcha_pictures[3]).click(captcha_pictures[3]).perform()
        captcha_attempts += 1
        driver.switch_to.default_content()
        wait(driver, 10, 1).until(
            ec.frame_to_be_available_and_switch_to_it((By.CLASS_NAME, "Z6wXJz-G-6vcu3I0ZoM_B")))
        wait(driver, 10, 1).until(
            ec.frame_to_be_available_and_switch_to_it((By.ID, "fc-iframe-wrap")))

    #
    # account_login_button = Wait(driver, 10, 0.5).until(EC.visibility_of_element_located((By.XPATH, button_after_login_xpath)))
    # AC(driver).move_to_element(account_login_button).perform()
    #
    # log_in_confirmation = driver.find_element_by_xpath(
    #     f"//div{div_account_after_successful_login}//span[@class='account-content__text']")
    #
    # if log_in_confirmation.text == expected_text_after_successful_login:
    #     print("Login by Email was successful")
    # else:
    #     print("Login by Email was unsuccessful !!!")


def login_by_facebook(driver):
    wait = WebDriverWait
    Id_FB_button = "login_by_fb"
    CSS_FB_cookie_button = 'button[data-cookiebanner="accept_button"]'
    Id_login_input = "email"
    Id_password_input = "pass"
    Id_login_button = "loginbutton"
    FB_login_value = "gil.lukasz@wp.pl"
    FB_password_value = "Vegeta11081984"
    expected_account_button_class = "account-standard account-standard--authorized"

    login_by_FB_button = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.ID, Id_FB_button)))
    ac(driver).move_to_element(login_by_FB_button).click(login_by_FB_button).perform()

    cookie_FB_button = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.CSS_SELECTOR, CSS_FB_cookie_button)))
    ac(driver).move_to_element(cookie_FB_button).click(cookie_FB_button).perform()

    login_input = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.ID, Id_login_input)))
    login_input.clear()
    login_input.send_keys(FB_login_value)

    password_input = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.ID, Id_password_input)))
    password_input.clear()
    password_input.send_keys(FB_password_value)

    login_button = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.ID, Id_login_button)))
    ac(driver).move_to_element(login_button).click(login_button).perform()

    account_button = wait(driver, 10, 1).until(
        ec.element_to_be_clickable((By.CSS_SELECTOR, "div.account-standard--authorized")))
    account_button_class = account_button.get_attribute("class")

    if account_button_class == expected_account_button_class:
        print("Login by Facebook was successful")
    else:
        print("Login by Facebook was unsuccessful !!!")


def login_by_gmail(driver):
    wait = WebDriverWait
    Id_GM_button = "login_by_g"
    Id_GM_cookie_button = "u_0_g"
    Id_login_input = "identifierId"
    Id_password_input = "pass"
    CSS_next_button = "#identifierNext  button"
    FB_login_value = "lukaszgil28@gmail.com"
    FB_password_value = "Vegeta11081984"
    expected_account_button_class = "account-standard account-standard--authorized"

    login_by_FB_button = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.ID, Id_GM_button)))
    ac(driver).move_to_element(login_by_FB_button).click(login_by_FB_button).perform()

    login_input = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.ID, Id_login_input)))
    login_input.clear()
    login_input.send_keys(FB_login_value)

    next_button = wait(driver, 10, 1).until(ec.element_to_be_clickable((By.CSS_SELECTOR, CSS_next_button)))
    ac(driver).move_to_element(next_button).click(next_button).perform()

    time.sleep(60)


def actual_article_title_visibility_try(driver, timeout, poll_frequency, locator):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, locator)))
        print('article title is visible')
    except:
        print('article title not visible')


def article_title_check_if(driver, locator, article_title, actual_article_title):
    actual_article_title = driver.find_element_by_css_selector(locator).get_attribute(
        "textContent")
    if article_title == actual_article_title:
        print(f"Current article title matches with expected title")
    else:
        print(f"Current article title does not match with expected")


def assert_page_url_if_with_exception(test_class_obj, driver, expected_page_url):
    actual_url = driver.current_url
    exception_url_TvnGo = "https://tvn24.pl/go/"

    if expected_page_url == actual_url:
        test_class_obj.assertEqual(expected_page_url, actual_url)
        print(f"Current url page: {driver.current_url} matches with expected url: {expected_page_url}")

    elif actual_url == exception_url_TvnGo:
        test_class_obj.assertEqual(exception_url_TvnGo, actual_url)
        print(f"Current url page: {driver.current_url} matches with expected url: {expected_page_url}")
    else:
        print(f"Expected {expected_page_url} differ from actual url on page: {driver.current_url}')")


def assert_page_url_try(test_class_obj, driver, expected_page_url):
    try:
        actual_url = driver.current_url
        test_class_obj.assertEqual(expected_page_url, actual_url)
    except AssertionError:
        print(f"Expected {expected_page_url} differ from actual url on page: {driver.current_url}')")
    finally:
        print(f"Your current page URL is : {driver.current_url}")


def assert_title_short(test_class_obj, driver, url, expected_page_title):
    driver.get(url)
    actual_title = driver.title
    test_class_obj.assertEqual(expected_page_title, actual_title,
                               f'Expected {expected_page_title} differ from actual title {actual_title} on page: {url}')

# def move_to_login_page_from_main_page1(driver):
#     Wait = WebDriverWait
#
#     CSS_account = "button.account-standard__toggle-button"
#     CSS_login = "div.account-standard__popup button.account-content__button.account-content__button--large"
#     CSS_roll_out_frame = "div.account-standard--visible"
#     expected_url = "account.tvn.pl"
#
#     attempts = 0
#     while attempts <= 10:
#
#         account = driver.find_element(By.CSS_SELECTOR, CSS_account)
#         AC(driver).move_to_element(account).perform()
#         roll_out_frames = driver.find_elements(By.CSS_SELECTOR, CSS_roll_out_frame)
#
#         if len(roll_out_frames) > 0:
#             log_in_button = roll_out_frames.find_element(By.CSS_SELECTOR, CSS_login)
#             AC(driver).move_to_element(log_in_button).click(log_in_button).perform()
#             Wait(driver, 10, 1).until(EC.url_contains(expected_url))
#             break
#         else:
#             time.sleep(0.5)
#             attempts += 1
# def move_to_login_page_from_main_page2(driver):
#     Wait = WebDriverWait
#
#     CSS_account = "button.account-standard__toggle-button"
#     CSS_login = "div.account-standard__popup button.account-content__button.account-content__button--large"
#     expected_url = "account.tvn.pl"
#
#     attempts = 0
#     while attempts <= 10:
#
#         account = driver.find_element(By.CSS_SELECTOR, CSS_account)
#         AC(driver).move_to_element(account).click(account).perform()
#         login_button = driver.find_element((By.CSS_SELECTOR, CSS_login))
#
#         if login_button.is_enabled():
#             AC(driver).move_to_element(login_button).click(login_button).perform()
#             Wait(driver, 10, 1).until(EC.url_contains(expected_url))
#             break
#         else:
#             time.sleep(0.5)
#             attempts += 1
