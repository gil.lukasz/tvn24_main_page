import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver import ActionChains as ac
from selenium.webdriver.support import select as se
from selenium.webdriver.common.keys import Keys

from selenium.common.exceptions import ElementNotVisibleException


def presence_try(driver, timeout, poll_frequency, locator, ):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, locator)))
        print('Element present')
    except:
        print('Element - not present')


def visibility_try(driver, timeout, poll_frequency, locator):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, locator)))
        print('Element visible')
    # except ElementNotVisibleException as ENVE:
    #     print(ENVE)
    except:
        print('Element - not visible')


def clickable_try(driver, timeout, poll_frequency, locator):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.element_to_be_clickable((By.CSS_SELECTOR, locator)))
        print('Element clickable')
    except:
        print('Element - not clickable')


def element_status_checks(driver, timeout, poll_frequency, locator):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, locator)))
        print(locator + '- present')
    except:
        print(locator + '- not present')

    try:
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, locator)))
        print(locator + '- visible')
    except:
        print(locator + '- not visible')

    try:
        wait.until(ec.element_to_be_clickable((By.CSS_SELECTOR, locator)))
        print(locator + '- clickable')
    except:
        print(locator + '- not clickable')


def mute_icon_presence_try(driver, timeout, poll_frequency, locator, ):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, locator)))
        print('Sound mute icon present')
    except:
        print('Sound icon NOT present')


def mute_icon_visibility_try(driver, timeout, poll_frequency, locator):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, locator)))
        print('Sound mute icon visible')

    except:
        print('Sound mute icon NOT visible')


def nuvi_bar_visibility_try(driver, timeout, poll_frequency, locator):
    wait = WebDriverWait(driver, timeout, poll_frequency)

    try:
        wait.until(ec.visibility_of_element_located((By.CSS_SELECTOR, locator)))
        print('nuvi control bar visible')

    except:
        print('nuvi control bar NOT visible')
