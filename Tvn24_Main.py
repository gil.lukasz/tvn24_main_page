import time
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains as ac
from selenium.webdriver.support import select as se

from Helpers import Tvn24_Methods as Tvn24m
from Helpers import Element_View_Status_Check as ES


class Tvn24_Menu(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.Url_Main = "https://tvn24.pl/"
        self.Url_Main_2 = "https://kontakt24.tvn24.pl/"
        self.Url_Main_3 = "https://tvn24.pl/premium"
        options = Options()
        options.page_load_strategy = 'normal'
        # options.add_argument('disable-infobars')
        # options.add_experimental_option("excludeSwitches", ['enable-automation'])
        # options.add_experimental_option('prefs', {'credentials_enable_service': False, 'profile': {
        #         'password_manager_enabled': False}})
        self.driver = webdriver.Chrome(
            executable_path=r'D:\Lukasz\chromedriver_88.exe', options=options)
        driver = self.driver
        driver.maximize_window()
        driver.get(self.Url_Main)
        Tvn24m.alert_accept_while_if_method(self.driver)
        time.sleep(4)

    @classmethod
    def tearDown(self):
        self.driver.quit()

    def test_Displayed_and_NotDisplayed_Menu_Elements_List(self):
        expected_url_from_button = f"{self.Url_Main}"

        driver = self.driver
        Tvn24m.assert_page_url_if_with_exception(self, self.driver, expected_url_from_button)
        Displayed_buttons = Tvn24m.displayed_buttons_txt_list(self.driver)
        Hidden_buttons = Tvn24m.not_displayed_buttons_txt_list(self.driver)

        print("Start Displayed")
        for button in Displayed_buttons:
            expected_url_from_button = f"{button}"
            driver.get(button)
            Tvn24m.alert_accept_try(self.driver, 5, 1)
            Tvn24m.assert_page_url_if_with_exception(self, self.driver, expected_url_from_button)

        print("Start Hidden")
        for button in Hidden_buttons:
            expected_url_from_button = f"{button}"
            driver.get(button)
            Tvn24m.alert_accept_try(self.driver, 5, 1)
            Tvn24m.assert_page_url_if_with_exception(self, self.driver, expected_url_from_button)

    def test_Main_Menu_hover_method(self):

        Tvn24m.hover_method_v1(self.driver)

    def test_Login_Page_Email(self):
        Tvn24m.move_to_account_v1(self.driver)
        Tvn24m.alert_accept_while_if_method(self.driver)
        Tvn24m.login_by_mail(self.driver)

    def test_Passed_login_FaceBook(self):
        Tvn24m.move_to_account_v2(self.driver)
        time.sleep(15)
        # Tvn24m.alert_accept_while_if_method(self.driver)
        # Tvn24m.login_by_facebook(self.driver)

    def test_login_Gmail(self):
        Tvn24m.move_to_account_v1(self.driver)
        Tvn24m.alert_accept_while_if_method(self.driver)
        Tvn24m.login_by_gmail(self.driver)

    def test_visibility_function_test(self):

        CSS_account = "button.account-standard__toggle-button"
        CSS_login = "div.account-standard__popup button.account-content__button.account-content__button--large"

        attempt_count = 1

        for attempt in range(50):
            print(f"Test : {attempt_count}")
            print("without slider")
            ES.presence_try(self.driver, 3, 0.5, CSS_login)
            ES.visibility_try(self.driver, 3, 0.5, CSS_login)

            account_button = self.driver.find_element_by_css_selector(CSS_account)
            ac(self.driver).move_to_element(account_button).click(account_button).perform()
            print("with slider")
            ES.visibility_try(self.driver, 3, 0.5, CSS_login)
            print("END")
            print("")
            attempt_count += 1
            self.driver.execute_script("location.reload(true);")

    def test_displayed_function_test(self):

        CSS_account = "button.account-standard__toggle-button"
        CSS_login = "div.account-standard__popup button.account-content__button.account-content__button--large"

        attempt_count = 1
        for attempt in range(50):

            print(f"Test : {attempt_count}")
            ES.presence_try(self.driver, 3, 0.5, CSS_login)
            account_button = self.driver.find_element_by_css_selector(CSS_account)
            login_button = self.driver.find_element_by_css_selector(CSS_login)

            if login_button.is_displayed():
                print("log_in button displayed - without slider")
                time.sleep(1)
                attempt_count += 1

            else:
                ac(self.driver).move_to_element(account_button).click(account_button).perform()
                time.sleep(1.5)
                if login_button.is_displayed():
                    print("log_in button displayed - with slider")
                    time.sleep(1.5)
                    attempt_count += 1
                else:
                    attempt_count += 1
                    pass
            print("END")
            print("")
            self.driver.execute_script("location.reload(true);")
            time.sleep(5)

    def test_video_article_main_page(self):

        articles_play_href_links = Tvn24m.find_articles_with_play_and_ellipseSound_buttons(self.driver)

        for article in articles_play_href_links:
            article_link = article
            self.driver.get(article_link)
            Tvn24m.alert_accept_while_if_method(self.driver)
            Tvn24m.video_ellipseSound_big_icon_hover(self.driver)
            time.sleep(10)
