from selenium.webdriver.common.by import By


class Tvn24_cookie_alert_locator:
    accept = (By.ID, "onetrust-accept-btn-handler")


class Tvn24_account_locator:
    account_frame = (By.CSS_SELECTOR, "li.header__list-item--account")
    account_login_button = (By.CSS_SELECTOR,
                            "div.account-standard__popup button.account-content__button.account-content__button--large")
    account_authorized = (By.CSS_SELECTOR, "div.account-standard--authorized")


class account_login_method_locator:
    Mail_button = (By.ID, "login_by_email")
    FB_button = (By.ID, "login_by_fb")
    Google_button = (By.ID, "login_by_g")
    Twitter_button = (By.ID, "login_by_tw")
    Apple_button = (By.ID, "login_by_apple")


class Login_By_Facebook_locator:
    Css_FB_cookie_accept = (By.CSS_SELECTOR, 'button[data-cookiebanner="accept_button"]')
    Id_login_input = (By.ID, "email")
    Id_password_input = (By.ID, "pass")
    Id_login_button = (By.ID, "loginbutton")
